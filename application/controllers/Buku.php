<?php
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Buku extends RestController
{
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('Buku_model', 'buku');
    }
    function index_get()
    {
        $id = $this->get('id');
        if ($id === null) {
            $allBuku = $this->buku->get();
            $this->response(
                // 'status' => true,
                  $allBuku
            , RestController::HTTP_OK);
        } else {
            $buku = $this->buku->get($id);
            if ($buku) {
                $this->response([
                    // 'status' => true,
                    // 'data' => $buku
                ], RestController::HTTP_OK);
            } else {
                $this->response([
                    // 'status' => false,
                    // 'msg' => $id . ' Tidak Ditemukan!'
                ], RestController::HTTP_NOT_FOUND);
            }
        }
    }
    function index_post()
    {
        $input = [
            'judul' => $this->post('judul'),
            'deskripsi' => $this->post('deskripsi')
        ];
        if ($this->buku->addBuku($input) > 0) {
            $this->response([
                // 'status' => true,
                // 'msg' => 'Buku berhasil ditambahkan'
            ], RestController::HTTP_CREATED);
        } else {
            $this->response([
                // 'status' => false,
                // 'msg' => 'Buku gagal ditambahkan'
            ], RestController::HTTP_BAD_REQUEST);
        }
    }
}
