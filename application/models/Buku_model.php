<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Buku_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function get($id = null)
    {
        if ($id === null) {
            $hasil = '';
            $hasil =  $this->db->get('buku')->result();
            return $hasil;
        } else {
            //echo $id;
            return $this->db->get_where('buku', ['id' => $id])->result_array();
        }
    }
    public function addBuku($data)
    {
        $this->db->insert('buku', $data);
        return $this->db->affected_rows();
    }
}
